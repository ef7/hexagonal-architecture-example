package com.baeldung.demo.api.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserResponse {
  private long id;
  private String name;
  private int age;
}
