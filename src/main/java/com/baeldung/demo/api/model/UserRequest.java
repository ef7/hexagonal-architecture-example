package com.baeldung.demo.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {
  private String name;
  private int age;
}
