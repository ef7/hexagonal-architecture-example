package com.baeldung.demo.api.controller;

import com.baeldung.demo.adapter.RestApiUserAdapter;
import com.baeldung.demo.api.model.UserRequest;
import com.baeldung.demo.api.model.UserResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
  // TODO log requests
  private RestApiUserAdapter restApiUserAdapter;

  @PostMapping
  public UserResponse addUser(@RequestBody UserRequest userRequest) {
    return restApiUserAdapter.addUser(userRequest);
  }

  @GetMapping("{id}")
  public UserResponse getUser(@PathVariable long id) {
    return restApiUserAdapter.getUserById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find user"));
  }
}
