package com.baeldung.demo.adapter;

import com.baeldung.demo.api.model.UserRequest;
import com.baeldung.demo.api.model.UserResponse;
import com.baeldung.demo.domain.model.User;
import com.baeldung.demo.domain.port.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class RestApiUserAdapter {
  private UserService userService;

  public UserResponse addUser(UserRequest userRequest) {
    User user = userService.addUser(new User(userRequest.getName(), userRequest.getAge()));
    return mapUserToUserResponse(user);
  }

  public Optional<UserResponse> getUserById(long id) {
    return userService.getUserById(id).map(this::mapUserToUserResponse);
  }

  private UserResponse mapUserToUserResponse(User user) {
    return new UserResponse(user.getId(), user.getName(), user.getAge());
  }
}
