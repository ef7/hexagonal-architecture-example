package com.baeldung.demo.adapter;

import com.baeldung.demo.domain.model.User;
import com.baeldung.demo.domain.port.UserRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DbUserRepository extends UserRepository, CrudRepository<User, Long> {
}
