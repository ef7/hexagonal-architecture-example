package com.baeldung.demo.domain.port;

import com.baeldung.demo.domain.model.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {
  private UserRepository userRepository;

  public User addUser(User user) {
    return userRepository.save(user);
  }

  public Optional<User> getUserById(long id) {
    return userRepository.findById(id);
  }
}
