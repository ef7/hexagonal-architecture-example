package com.baeldung.demo.domain.port;

import com.baeldung.demo.domain.model.User;

import java.util.Optional;

public interface UserRepository {
  User save(User user);
  Optional<User> findById(long id);
}
